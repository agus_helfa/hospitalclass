<?php
namespace Helfa\HospitalClass\RSPersahabatan\Services;

use Helfa\HospitalClass\RSPersahabatan\RSService;
use App\Models\HospitalPoliklinik;

class Bpjs extends RSService
{
    public function getRujukan($data)
    {
        $params = [
            "no_rm" => $data["mrnumber"],
            "tgl" => $data["birth_date"],
            "rujukan" => $data["referral_number"]
        ];
        $env = $this->environment();
        $response = $this->post(($env == 'production') ? 'pencarianPasien' : 'pencarianpasien_post', $params);
        $jarray = json_decode($response, true);

        if ($jarray["Code"] == "200"){
            if (empty($jarray["Data"][0]["no_rujukan"])){
                $result = [
                    "metadata" => [
                        "code" => 403,
                        "message" => 'Rujukan tidak ditemukan',
                    ],
                    "data" => []
                ];
            }else{
                $poliRs = HospitalPoliklinik::selectRaw("int_code as policlinic_id, logo, nama_gedung || ' - ' || nama_poliklinik || (case when sub_poliklinik is null then '' else ' - ' || sub_poliklinik end ) as policlinic_name, show_doctor, show_time")
                                                ->where('smf_id', $jarray["Data"][0]["poli"])
                                                ->where('instalasi_id', $jarray["Data"][0]["instalasi"])
                                                ->where('hospital_code', (string)$data['hospital_code'])->get();

                $result = [
                    "metadata" => [
                        "code" => 200
                    ],
                    "data" => [
                            "referral_number" => $jarray["Data"][0]["no_rujukan"],
                            "referral_date" => $jarray["Data"][0]["tglKunjungan"],
                            "referral_policlinic" => $poliRs,
                            "rsklinik" => $jarray["Data"][0]["rsKlinik"],
                            "instalasi_id" => $jarray["Data"][0]["instalasi"],
                            "kdfaskes" => null,
                            "namafaskes" => $jarray["Data"][0]["rsFaskes"],
                            "day_left" => (!empty($jarray["Data"][0]["selisih_waktu"])) ? $jarray["Data"][0]["selisih_waktu"] : 0,
                            "referral_count" => (!empty($jarray["Data"][0]["no_rujukan_sudah_digunakan"])) ? $jarray["Data"][0]["no_rujukan_sudah_digunakan"] : 0,
                    ],
                ];
            }
        }else{
            $result = [
                "metadata" => [
                    "code" => 404,
                    "message" => (!isset($jarray['Pesan'])) ? "Rujukan tidak ditemukan" : $jarray['Pesan'],
                ],
                "data" => [],
            ];
        }

        return $result;
    }

}
