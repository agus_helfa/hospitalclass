<?php
namespace Helfa\HospitalClass\RSPersahabatan\Services;

use Helfa\HospitalClass\RSPersahabatan\RSService;
use App\Models\HospitalPoliklinik;
use Carbon\Carbon;

class Rajal extends RSService
{
    public function createRajal($data)
    {
        $param = [
            "no_rm" => $data['mrnumber'],
            "tgl" => $data['register_date'],
            "kode_poli" => $data['policlinic_id'],
            "smf" => (int)substr((string)$data['policlinic_id'],1,2),
            "instalasi" => (string)$data['instalasi_id'],
            "tanggal_kunjungan" => $data['visit_date'],
            "cara_bayar" => $this->insurance_mappingReturn($data['payment_method_id']),
            "nomor_kontak_perjanjian" => $data['contact_number'],
            "nama_pasien" => $data['patient_name'],
            "email_pasien" => $data['patient_email'],
            "no_rujukan_faskes" => $data['referral_number'],
            "tgl_rujukan_faskes" => $data['referral_date'],
            "no_kartu_penjamin" => $data['insurance_number']
        ];

        $env = $this->environment();
        $response = $this->post(($env == 'production') ? 'savePendaftaranPerjanjian' : 'savePendaftaran_post', $param, ['connect_timeout' => 3.14]);
        $jarray = json_decode($response, true);

        if ($jarray['Data']['kode_unik']){
            $result = [
                "metadata" => [
                    "code" => (int)$jarray["Code"],
                    "message" => $jarray["pesan"],
                ],
                "data" => [
                    "token_number" => $jarray['Data']['kode_unik'],
                    "queue_number" => $jarray['Data']['antrian'],
                    "visit_time" => null,
                ],
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => (int)$jarray["Code"],
                    "message" => $jarray["pesan"],
                ],
                "data" => [],
            ];
        }

        return $result;
    }

    public function getByTgl($data)
    {
        $param = [
            "no_rm" => $data['mrnumber'],
            "tgl" => $data['visit_date']
        ];

        $response = $this->post('pencarianHistoriPerjanjian', $param);
        $jarray = json_decode($response, true);

        $resultArray = [];
        foreach ($jarray['Data'] as $key => $hasil) {
            $poli = HospitalPoliklinik::where('hospital_code',$data['hospital_code'])
                ->where('instalasi_id',$hasil['instalasi'])
                ->where('smf_id',$hasil['smf'])->first();

            $resultArray[] =[
                "token_number" => $hasil['kode_unik_generate'],
                "mrnumber" => $hasil['no_rm'],
                "patient_name" => $hasil['nama_pasien_perjanjian'],
                "visit_date" => date('Y-m-d',strtotime($hasil['tanggal_kunjungan'])),
                "policlinic_id" => $poli->int_code,
                "policlinic_name" => $poli->nama_poliklinik,
                "status" => $this->status_mapping($hasil['status']),
                "payment_method_id" => $this->insurance_mapping($hasil['cara_bayar']),
                "queue_number" => $hasil['antrian'],
                "instalasi_id" => $hasil['instalasi'],
                "insurance_number" => $hasil['no_kartu_penjamin'],
            ];
        }

        $result = [
            "metadata" => [
                "code" => (int)$jarray["Code"],
            ],
            "data" => $resultArray
        ];

        return $result;

    }

    //registrasi rawat jalan
    public function confirmVisit($data)
    {
        $param = [
            "kode_unik_generate" => $data['token']
        ];

        $env = $this->environment();
        $response = $this->post(($env == 'production') ? 'SavePendaftaranOnline' : 'save_pendaftaran_online', $param, ['connect_timeout' => 3.14]);
        $jarray = json_decode($response, true);

        if($jarray["Code"]){
            $poli = HospitalPoliklinik::where('hospital_code',$data['hospital_code'])
                    ->where('instalasi_id',$jarray['Data']['instalasi'])
                    ->where('smf_id',$jarray['Data']['smf'])->first();
            $result = [
                "metadata" => [
                    "code" => (int)$jarray["Code"],
                    "message" => $jarray["pesan"],
                ],
                "data" => [
                    "sep_number"=> $jarray['Data']['sep'],
                    "patient_name"=> $jarray['Data']['nama'],
                    "mrnumber"=> $jarray['Data']['no_rm'],
                    "sep_date"=> $jarray['Data']['tgl_sep'],
                    "patient_birthdate"=> $jarray['Data']['tgl_lahir'],
                    "regnumber"=> $jarray['Data']['no_reg'],
                    "instalasi_id"=> $jarray['Data']['instalasi'],
                    "smf_id"=> $jarray['Data']['smf'],
                    "faskes"=> $jarray['Data']['faskes'],
                    "referral_diagnosis"=> $jarray['Data']['diagnosa'],
                    "jenis_peserta"=> $jarray['Data']['peserta'],
                    "referral_cob"=> $jarray['Data']['COB'],
                    "jenis_rawat"=> $jarray['Data']['jenis_rawat'],
                    "kelas_rawat"=> $jarray['Data']['kelas_rawat'],
                    "lakalantas"=> $jarray['Data']['Lakalantas'] == null ? '':$jarray['Data']['Lakalantas'],
                    "queue_number"=> $jarray['Data']['antrian'],
                    "policlinic_name"=> $poli ? $poli->nama_poliklinik:'',
                    "policlinic_id"=> $poli ? $poli->int_code:0,
                    "insurance_number" => $jarray['Data']['no_kartu_penjamin'],
                    "payment_method_id" => $this->insurance_mapping($jarray['Data']['cara_bayar']),
                    "sub_polyclinic" => $jarray['Data']['nmPoliBPJS'],
                    "visit_date"=> $jarray['Data']['tgl_sep'],
                    "patient_id"=> null,
                    "referralnumber_bpjs"=> '',
                    "doctor_name" => null,
                    "start_time" => null,
                    "end_time" => null,
                ],
            ];
        }

        return $result;
    }
}
