<?php
namespace Helfa\HospitalClass\RSPersahabatan\Services;

use Helfa\HospitalClass\RSPersahabatan\RSService;
  
class Pasien extends RSService
{
    public function getByNoKartu($norm,$tgllahir)
    {
        $data = [
            "no_rm" => $norm,
            "tgl" => $tgllahir
        ];

        $env = $this->environment();
        $response = $this->post(($env == 'production') ? 'pencarianPasien' : 'pencarianpasien_post', $data);
        $jarray = json_decode($response, true);

        if ($jarray["Code"] == "200"){
            $result = [
                "metadata" => [
                    "code" => 200
                ],
                "data" => [
                        "mrnumber" => $jarray["Data"][0]["no_rm"],
                        "ext_id" => null,
                        "name" => $jarray["Data"][0]["nama"],
                        "gender"=> $jarray["Data"][0]["jenis_kelamin"],
                        "telephone" => null,
                        "birth_date" => $tgllahir,
                        "bpjs_number" => $jarray["Data"][0]["no_kartu_jaminan"],
                    // "bpjs" => [
                    //     "bpjs_number" => $jarray["Data"][0]["no_kartu_jaminan"],
                    // ],
                    // "rujukan_bpjs" => [
                    //     "no" => $jarray["Data"][0]["no_rujukan"],
                    //     "kdpolirs" => $jarray["Data"][0]["poli"],
                    //     "namapolirs" => $jarray["Data"][0]["rsKlinik"],
                    //     "tglkunjungan" => $jarray["Data"][0]["tglKunjungan"],
                    //     "faskes" => $jarray["Data"][0]["rsFaskes"],
                    // ],
                ]
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => 404
                ],
                "data" => [],
            ];
        }
        
        return $result;
    }

}