<?php
namespace Helfa\HospitalClass\RSPersahabatan\Services;

use Helfa\HospitalClass\RSPersahabatan\RSService;
use App\Models\HospitalPoliklinik;

class PoliklinikRS extends RSService
{

    public function getByTgl($data)
    {
        $hospital = (string)$data['hospital_code'];
        $payment_method = (string)$data['payment_method'];

        $poli = HospitalPoliklinik::where('aktif', 1)->where('hospital_code', $hospital)->orderby('nama_gedung')->orderby('nama_poliklinik')->get();

        if (count($poli) > 0){
            $resultArray = [];
            foreach ($poli as $key => $hasil) {
                $sub = empty($hasil['sub_poliklinik']) ? '' : ' - ' . $hasil['sub_poliklinik'];
                $resultArray[] =[
                    "policlinic_id" => $hasil['int_code'],
                    "policlinic_name" => $hasil['nama_gedung'] . ' - ' . $hasil['nama_poliklinik'] . $sub,
                    "instalasi_id" => (string)$hasil['instalasi_id'],
                    "logo" => $hasil['logo'],
                    "show_doctor" => $hasil['show_doctor'],
                    "show_time" => $hasil['show_time']
                ];
            }

            $result = [
                "metadata" => [
                    "code" => 200
                ],
                "data" => $resultArray
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => 404
                ],
                "data" => []
            ];
        }

        return $result;

    }

}
