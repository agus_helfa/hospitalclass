<?php
namespace Helfa\HospitalClass\RSPersahabatan\Services;

use Helfa\HospitalClass\RSPersahabatan\AntrianService;
 
class Antrian extends AntrianService
{ 
    public function postQueue($data)
    {
        $param = [
            
            "department_id" => $data["policlinic_id"],
            "counter_id" => $data["sub_policlinic_id"],
            "number" => $data["queue_number"],
            "date1" => $data["visit_date"],
            "nama" => $data["patient_name"],
            "nomr" => $data["mrnumber"],
            "register_number" => $data["token_number"],
        ];

        $response = $this->post('addtoqueue', $param);
        $jarray = json_decode($response, true);

        $hasil = $jarray['id'];

        if (!empty($hasil)){
            $result = [
                "metadata" => [
                    "code" => 200,
                    "message" => "ok",
                ],
                "data" => [
                    $jarray
                ], 
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => 403,
                    "message" => "error",
                ],
                "data" => [], 
            ];
        }
        
        
        return $result;
    }

    
 
}