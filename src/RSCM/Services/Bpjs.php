<?php
namespace Helfa\HospitalClass\RSCM\Services;

use Helfa\HospitalClass\RSCM\RSService;
use App\Models\HospitalPoliklinik;

class Bpjs extends RSService
{
    public function getRujukan($data)
    {
        $datareq = [
            "fungsi" => "checkRujukanBPJS",
            "appointment_date" => $data['visit_date'],
            "no_bpjs" => $data['bpjs_number'],
            "birth_date" => $data['birth_date']
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response,true);

        if ($jarray['status'] == 200){
            if (array_key_exists("error_text",$jarray['data'])){
                $result = [
                    "metadata" => [
                        "code" => 403,
                        "message" => $jarray['data']['error_text']
                    ],
                    "data" => []
                ];
            }else{
                $result = [
                    "metadata" => [
                        "code" => 200
                        ]
                ];

                $data_reponse=[];
                foreach($jarray["data"] as $key => $value){
                    $poliRs = HospitalPoliklinik::selectRaw("int_code as policlinic_id, logo, nama_gedung || ' - ' || nama_poliklinik || (case when sub_poliklinik is null then '' else ' - ' || sub_poliklinik end ) as policlinic_name, show_doctor, show_time")
                        ->where('bpjs_code', $value["poliRujukan"]["kode"])
                        ->where('hospital_code', $data['hospital_code'])->get();

                    $data_reponse[] = ["referral_number" => $value["no_rujukan"],
                        "referral_date" => $value["tgl_rujukan"],
                        "referral_policlinic" => $poliRs,
                        "rsklinik" => $value["poliRujukan"]["nama"],
                        "kdfaskes" => $value["kode_rs"],
                        "namafaskes" => $value["nama_rs"],
                        "instalasi_id" => null,
                        "referral_count" => null,
                        "day_left" => null
                    ];
                }
                $result["data"] = $data_reponse;
            }

        }else{
            $result = [
                "metadata" => [
                    "code" => 404
                ],
                "data" => [],
            ];
        }

        return $result;

    }

}
