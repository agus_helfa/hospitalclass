<?php
namespace Helfa\HospitalClass\RSCM\Services;

use Helfa\HospitalClass\RSCM\RSService;

class DokterRS extends RSService
{
    public function getByPoliDate($data)
    {
        $datareq = [
            "fungsi" => "getJadwalDokter",
            "appointment_date" => $data['visit_date'],
            "poli_id" => $data['policlinic_id']
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response, true);

        if ($jarray["status"] == 200){

            $result = [
                "metadata" => [
                    "code" => 200]
                ];

            $dokter = [];
            foreach ($jarray['data'] as $key => $hasil){
                $getDoctorID = explode('/',$hasil["actor"][0]['reference']);
                $dokter[] = [
                    "schedule_id" => $hasil["id"],
                    "doctor_id" => $getDoctorID[1],
                    "doctor_name" => $hasil["actor"][0]['display'],
                    "times" => [$hasil["planningHorizon"]]
                ];
            };

            $result['data'] = $dokter;

        }else{
            $result = [
                "metadata" => [
                    "code" => 404
                ],
                "data" => [],
            ];
        }

        return $result;
    }

}
