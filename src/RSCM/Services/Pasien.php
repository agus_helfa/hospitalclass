<?php
namespace Helfa\HospitalClass\RSCM\Services;

use Helfa\HospitalClass\RSCM\RSService;
  
class Pasien extends RSService
{
    public function getByNoKartu($norm,$tgllahir)
    {
        $datareq = [
            "fungsi" => "getPatientInfo",
            "birth_date" => $tgllahir,
            "mrn" => $norm,
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response, true);

        if ($jarray["status"] == 200){
            $result = [
                "metadata" => [
                    "code" => 200
                ],
                "data" => [
                        "mrnumber" => str_replace('-','',$jarray["data"]["identifier"][0]["value"]),
                        "ext_id" => null,
                        "name" => $jarray["data"]["name"]["text"],
                        "gender"=> null,
                        "telephone" => $jarray["data"]["telecom"][0]["value"],
                        "birth_date" => $jarray["data"]["birthDate"],
                        "address" => $jarray["data"]["address"][0]["text"],
                        "bpjs_number" => $jarray["data"]["identifier"][1]["value"]
                ],
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => 404
                ],
                "data" => [],
            ];
        }
        
        return $result;

    }

}