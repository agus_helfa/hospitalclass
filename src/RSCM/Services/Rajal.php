<?php
namespace Helfa\HospitalClass\RSCM\Services;

use Carbon\Carbon;
use Helfa\HospitalClass\RSCM\RSService;
use App\Models\HospitalPoliklinik;

class Rajal extends RSService
{
    public function createRajal($data)
    {
        $param = [
            "appointment_date" => $data['visit_date'],
            "mrn" => $data['mrnumber'],
            "poli_id" => $data['policlinic_id'],
            "dokter_id" => $data['doctor_id'],
            "jaminan" => $this->insurance_mappingReturn($data['payment_method_id']),
            "no_telpon" => $data['contact_number'],
            "no_bpjs" => $data['insurance_number'],
            "no_rujukan" => $data['referral_number'],
            "tgl_rujukan" => $data['referral_date'],
            "schedule_id" => $data['schedule_id'],
            "email" => $data['patient_email'],
            "fungsi" => "kirimPerjanjianOnline",
        ];

        $response = $this->post('api.php', $param);

        $jarray = json_decode($response, true);

        if ($jarray["status"] == 200){
            $result = [
                "metadata" => [
                    "code" => 200
                ],
                "data" => [
                    "token_number" => $jarray['data']['NomorReservasi'],
                    "queue_number" => null,
                    "visit_time" => $jarray['data']['JamKunjungan'],
                ],
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => (int)$jarray["status"],
                    "message" => $jarray["data"]["text"],
                ],
                "data" => [],
            ];
        }

        return $result;

    }

    public function getByTgl($data)
    {
        $start_date_ori = new Carbon($data['visit_date']);
        $end_date = $start_date_ori->addMonths(12);

        $datareq = [
            "fungsi" => "laporanPerjanjianHELFAbyMRN",
            "start_dttm" => $data['visit_date'],
            "end_dttm" => $end_date->toDateString(),
            "mrn" => $data['mrnumber'],
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response, true);
        $resultArray=[];
        foreach ($jarray['data'] as $key => $hasil) {
            $poli = HospitalPoliklinik::where('hospital_code',$data['hospital_code'])
                ->where('int_code',$hasil['kd_poli'])->first();

            $resultArray[] =[
                "token_number" => $hasil['no_perjanjian'],
                "mrnumber" => str_replace('-','',$hasil['norm']),
                "patient_name" => $hasil['patient_nm'],
                "visit_date" => date('Y-m-d',strtotime($hasil['tgl_visit'])),
                "policlinic_id" => $poli->int_code,
                "policlinic_name" => $poli->nama_poliklinik,
                "status" => $this->status_mapping($hasil['status']),
                "payment_method_id" => $this->insurance_mapping($hasil['jaminan']),
                "queue_number" => null,
                "instalasi_id" => null,
                "insurance_number" => null,
            ];
        }

        $result = [
            "metadata" => [
                "code" => (int)$jarray["status"]
            ],
            "data" => $resultArray
        ];

        return $result;

    }

    public function getAllByTgl($data)
    {
        $datareq = [
            "fungsi" => "laporanPerjanjianHELFA",
            "tgl_kedatangan" => $data['visit_date']
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response, true);

        $resultArray = [];
        foreach ($jarray['data'] as $key => $hasil) {
            $resultArray[] =[
                "regnumber" => $hasil['no_perjanjian'],
                "mrnumber" => $hasil['norm'],
                "name" => null,
                "visit_date" => date('Y-m-d',strtotime($hasil['tgl_visit'])),
                "policlinic_id" => $hasil['kd_poli'],
                "policlinic_name" => $hasil['nama_poli'],
                "status" => $this->status_mapping($hasil['status']),
                "insurance" => $this->insurance_mapping($hasil['jaminan']),
                "queue_number" => null
            ];
        }

        $result = [
            "metadata" => [
                "code" => 200
            ],
            "data" => $resultArray
        ];

        return $result;

    }

    //registrasi rawat jalan
    public function confirmVisit($data)
    {
        $datareq = [
            "fungsi" => "createSEPPerjanjian",
            "perjanjian_id" => $data['token']
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response, true);

        if ((int)$jarray["status"] == 200){
            $result = [
                "metadata" => [
                    "code" => (int)$jarray["status"],
                    "message" => $jarray["pesan"],
                ],
                "data" => [
                    "sep_number"=> $jarray['data']['noSep'],
                    "patient_name"=> (isset($jarray['data']['jaminan']) && $jarray['data']['jaminan']=='NON JKN') ? $jarray['data']['namaPeserta'] : $jarray['data']['peserta']['nama'],
                    "mrnumber"=> (isset($jarray['data']['jaminan']) && $jarray['data']['jaminan']=='NON JKN') ? str_replace('-','',$jarray['data']['mrn']) : str_replace('-','',$jarray['data']['peserta']['noMr']),
                    "sep_date"=> $jarray['data']['tglSep'],
                    "patient_birthdate"=> $jarray['data']['peserta']['tglLahir'],
                    "regnumber"=> str_replace('/','',$jarray['data']['noReg']),
                    "instalasi_id"=> null,
                    "smf_id"=> null,
                    "faskes"=> null,
                    "referral_diagnosis"=> $jarray['data']['diagnosa'],
                    "jenis_peserta"=> $jarray['data']['peserta']['jnsPeserta'],
                    "referral_cob"=> null,
                    "jenis_rawat"=> $jarray['data']['jnsPelayanan'],
                    "kelas_rawat"=> $jarray['data']['peserta']['hakKelas'],
                    "lakalantas"=> null,
                    "queue_number"=> null,
                    "policlinic_name"=> $jarray['data']['poli_nm'],
                    "policlinic_id"=> $jarray['data']['poli_id'],
                    "insurance_number" => $jarray['data']['peserta']['noKartu'],
                    "payment_method_id" => (isset($jarray['data']['jaminan']) && $jarray['data']['jaminan']=='NON JKN') ? $this->insurance_mapping($jarray['data']['jaminan']) :  $this->insurance_mapping('JKN'),
                    "sub_polyclinic" => null,
                    "visit_date"=> $jarray['data']['tglSJP'],
                    "patient_id"=> $jarray['data']['patient_id'],
                    "referralnumber_bpjs"=> $jarray['data']['noRujukan'],
                    "doctor_name" => $jarray['data']['dokter_nm'],
                    "start_time" => $jarray['data']['jam_awal'],
                    "end_time" => $jarray['data']['jam_akhir'],
                ]
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => (int)$jarray["status"],
                    "message" => $jarray["pesan"],
                ],
                "data" => []
            ];
        }

        return $result;
    }

}
