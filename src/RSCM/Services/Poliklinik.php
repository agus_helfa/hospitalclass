<?php
namespace Helfa\HospitalClass\RSCM\Services;

use Helfa\HospitalClass\RSCM\RSService;
use App\Models\HospitalPoliklinik;

class PoliklinikRS extends RSService
{
    public function getByTgl($data)
    {
        $hospital = (string)$data['hospital_code'];

        $datareq = [
            "fungsi" => "getJadwalPoli",
            "appointment_date" => $data['visit_date']
        ];

        $response = $this->post('api.php', $datareq);

        $jarray = json_decode($response, true);

        if ($jarray["status"] == 200){
            $resultArray = [];
            foreach ($jarray['data'] as $key => $hasil) {
                $logoPoli = HospitalPoliklinik::select('instalasi_id','logo','show_doctor','show_time')
                        ->where('hospital_code', $hospital)
                        ->where('int_code',(string)$hasil['id'])
                        ->where('aktif',1)
                        ->first();

                if(!empty($logoPoli)){
                    $resultArray[] =[
                        "policlinic_id" => $hasil['id'],
                        "policlinic_name" => $hasil['name'],
                        "instalasi_id" => (string)$logoPoli->instalasi_id,
                        "logo" => $logoPoli->logo,
                        "show_doctor" => $logoPoli->show_doctor,
                        "show_time" => $logoPoli->show_time
                    ];
                }
            }
            $result = [
                "metadata" => [
                    "code" => 200
                ],
                "data" => $resultArray
            ];
        }else{
            $result = [
                "metadata" => [
                    "code" => 404
                ],
                "data" => []
            ];
        }

        return $result;

    }

}
