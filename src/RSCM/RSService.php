<?php
namespace Helfa\HospitalClass\RSCM;

use GuzzleHttp\Client;

class RSService{

    /**
     * Guzzle HTTP Client object
     * @var \GuzzleHttp\Client
     */
    private $clients;

    /**
     * Request headers
     * @var array
     */
    private $headers;

    /**
     * @var string
     */
    private $secret_key;

    /**
     * @var string
     */
    private $base_url;

    /**
     * @var string
     */
    private $service_name;

    /**
     * @var string
     */
    private $user_nm;

    /**
     * Request body
     * @var array
     */
    private $body;

    public function __construct($configurations)
    {
        $this->clients = new Client([
            'verify' => false
        ]);

        foreach ($configurations as $key => $val){
            if (property_exists($this, $key)) {
                $this->$key = $val;
            }
        }

        //set body parameters
        $this->setBody();
    }

    protected function setBody()
    {
        $this->body = [
            'user_nm' => $this->user_nm,
            'key' => $this->secret_key
        ];
        return $this;
    }

    protected function post($feature, $data = [], $headers = [])
    {
        $this->headers['Content-Type'] = 'application/json';
        if(!empty($headers)){
            $this->headers = array_merge($this->headers,$headers);
        }
        try {
            $response = $this->clients->request(
                'POST',
                $this->base_url . '/' . $this->service_name . '/' . $feature,
                [
                    'headers' => $this->headers,
                    'json' => array_merge($this->body, $data),
                ]
            )->getBody()->getContents();
        } catch (\Exception $e) {
            $response = $e->getResponse()->getBody();
        }
        return $response;
    }

    protected function insurance_mapping($data)
    {
        $insurance_helfa = [
            'JKN' => '2',
            'NON JKN' => '1',
            'tunai' => '1'
        ];

        return $insurance_helfa[$data];
    }

    protected function insurance_mappingReturn($data)
    {
        $insurance_helfa = [
            '2' => 'JKN',
            '1' => 'NON JKN',
            '3' => 'NON JKN'
        ];

        return $insurance_helfa[$data];
    }

    protected function status_mapping($data)
    {
        $status = [
            //rspersahabatan  => helfa
            'batal' => 5,
            'datang' => 1,  //menunggu
            'menunggu' => 6 //perjanjian
        ];

        return $status[$data];
    }

}
